# Api

### Primeiros Passos

• Instale de prefência o MySQL Workbench
• Após ter instalado;
• Abra o MySQL Workbench 
e realize a conexão com os seguintes dados abaixo;

• Adicione os seguintes elementos abaixo nos campos determinado;
• host:localhost
• user:root
• password: root
• port:3306

• Após realizar a conexão execute o script que esta abaixo de todos os passos no Workbench para criar o banco e inserir um usuário;

• Logo após;
• Fazer um clone do projeto com `git clone`;
• Dentro do seu projeto no terminal digite `npm install` para baixar as dependências do projeto;
• Para rodar o projeto digite `npm start` no seu terminal do projeto e pronto;

### Script

---

-- Schema mydb

---

---

-- Schema mydb

---

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

CREATE TABLE IF NOT EXISTS `mydb`.`colaboradores` (
  `idcolaboradores` INT(11) NOT NULL AUTO_INCREMENT,
  `nome_completo` VARCHAR(45) NULL DEFAULT NULL,
  `cpf` VARCHAR(12) NOT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `data_nasc` DATE NULL DEFAULT NULL,
  `endereco` VARCHAR(45) NULL DEFAULT NULL,
  `numero` VARCHAR(45) NULL DEFAULT NULL,
  `data_admissao` DATE NULL DEFAULT NULL,
  `telefone_celular` VARCHAR(45) NULL DEFAULT NULL,
  `setor` VARCHAR(45) NULL DEFAULT NULL,
  `senha` VARCHAR(100) NOT NULL,
  `horas_mensais` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`idcolaboradores`))
ENGINE = InnoDB
AUTO_INCREMENT = 82
DEFAULT CHARACTER SET = utf8;

INSERT INTO `colaboradores` (nome_completo, cpf, email, data_nasc, endereco, numero, data_admissao, telefone_celular, setor, senha, horas_mensais) VALUES ('Teste','43219323006','teste@gmail.com','1999-01-15','Rua teste','111','2020-10-23','991785504','RH','12345678',160) ;

CREATE TABLE IF NOT EXISTS `mydb`.`pontos` (
  `num_registro` INT(11) NOT NULL AUTO_INCREMENT,
  `data` DATETIME NOT NULL,
  `colaboradores_idcolaboradores` INT(11) NOT NULL,
  `entrada` TIME NULL DEFAULT NULL,
  `saida` TIME NULL DEFAULT NULL,
  PRIMARY KEY (`num_registro`),
  INDEX `fk_pontos_colaboradores1_idx` (`colaboradores_idcolaboradores` ASC),
  CONSTRAINT `fk_pontos_colaboradores1`
    FOREIGN KEY (`colaboradores_idcolaboradores`)
    REFERENCES `mydb`.`colaboradores` (`idcolaboradores`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 287
DEFAULT CHARACTER SET = utf8;


CREATE TABLE IF NOT EXISTS `mydb`.`solicitacoes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `data` DATE NULL DEFAULT NULL,
  `entrada` TIME NULL DEFAULT NULL,
  `saida` TIME NULL DEFAULT NULL,
  `observacao` LONGTEXT NULL DEFAULT NULL,
  `colaboradores_idcolaboradores` INT(11) NOT NULL,
  `pontos_num_registro` INT(11) NOT NULL,
  `edit` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_solicitacoes_colaboradores1_idx` (`colaboradores_idcolaboradores` ASC),
  INDEX `fk_solicitacoes_pontos1_idx` (`pontos_num_registro` ASC),
  CONSTRAINT `fk_solicitacoes_colaboradores1`
    FOREIGN KEY (`colaboradores_idcolaboradores`)
    REFERENCES `mydb`.`colaboradores` (`idcolaboradores`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_solicitacoes_pontos1`
    FOREIGN KEY (`pontos_num_registro`)
    REFERENCES `mydb`.`pontos` (`num_registro`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 101
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `tarefas_projetos`;

CREATE TABLE IF NOT EXISTS `mydb`.`tarefas_projetos` (
  `id_tarefas_projetos` INT(11) NOT NULL AUTO_INCREMENT,
  `data_inicio` DATE NULL DEFAULT NULL,
  `hora` TIME NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `colaboradores_idcolaboradores` INT(11) NOT NULL,
  `data_termino` DATE NULL DEFAULT NULL,
  `hora_termino` TIME NULL DEFAULT NULL,
  `descricao` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_tarefas_projetos`),
  INDEX `fk_tarefas_projetos_colaboradores1_idx` (`colaboradores_idcolaboradores` ASC),
  CONSTRAINT `fk_tarefas_projetos_colaboradores1`
    FOREIGN KEY (`colaboradores_idcolaboradores`)
    REFERENCES `mydb`.`colaboradores` (`idcolaboradores`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;
