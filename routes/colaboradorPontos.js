const express = require("express");
const router = express.Router();
const mysql = require("../mysql").pool;
const loginTwo = require("../middleware/loginTwo.js");
const dayjs = require("dayjs");

//============ Cadastro de Ponto  =======================
router.post("/", loginTwo, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    const dataNova = new Date();

    conn.query(
      `SELECT * FROM pontos WHERE colaboradores_idcolaboradores = ? order by data desc `,
      [req.body.colaboradores_idcolaboradores],
      (error, resultado, field) => {
        if (error) {
          return res.status(500).json({ error: error });
        }
        if (resultado == 0) {
          conn.query(
            "INSERT INTO pontos (data, colaboradores_idcolaboradores, entrada, saida) VALUES (?,?,?,?)",
            [dataNova, req.body.colaboradores_idcolaboradores, dataNova, null],
            (error, resultado, field) => {
              conn.release();

              error && res.status(500).send({ error: error });

              res.status(201).send({
                mensagem: "Ponto cadastrado com sucesso!",
                id: resultado.insertId,
              });
            }
          );
        } else {
          if (resultado[0].saida != null) {
            conn.query(
              "INSERT INTO pontos (data, colaboradores_idcolaboradores, entrada, saida) VALUES (?,?,?,?)",
              [
                dataNova,
                req.body.colaboradores_idcolaboradores,
                dataNova,
                null,
              ],
              (error, resultado, field) => {
                conn.release();

                error && res.status(500).send({ error: error });

                res.status(201).send({
                  mensagem: "Ponto cadastrado com sucesso!",
                  id: resultado.insertId,
                });
              }
            );
          } else {
            conn.query(
              "UPDATE pontos SET saida = ? WHERE colaboradores_idcolaboradores = ? AND entrada IS NOT NULL AND saida IS NULL",
              [dataNova, req.body.colaboradores_idcolaboradores],
              (error, resultado, field) => {
                conn.release();

                error && res.status(500).send({ error: error });

                res.status(201).send({
                  mensagem: "Ponto cadastrado com sucesso!",
                  id: resultado.insertId,
                });
              }
            );
          }
        }
      }
    );
  });
});

//====================== Retorna os Pontos de um único colaborador ================
router.get("/id/:id", loginTwo, (req, res, next) => {
  const id = req.params.id;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT pontos.num_registro, colaboradores.nome_completo, pontos.colaboradores_idcolaboradores, pontos.data, pontos.entrada, pontos.saida, colaboradores.idcolaboradores FROM pontos JOIN colaboradores on colaboradores.idcolaboradores=pontos.colaboradores_idcolaboradores WHERE idcolaboradores LIKE '${id}' ORDER BY data asc `,
      [req.body.colaboradores_idcolaboradores],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).json({ error: error });
        }
        res.status(201).json({ response: resultado });
      }
    );
  });
});

//============== Retorna Somente um ponto ================
router.get("/numeroRegistro/:num", loginTwo, (req, res, next) => {
  const { num } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT pontos.num_registro, colaboradores.nome_completo, pontos.colaboradores_idcolaboradores, pontos.data, pontos.entrada, pontos.saida, colaboradores.idcolaboradores FROM pontos JOIN colaboradores on colaboradores.idcolaboradores=pontos.colaboradores_idcolaboradores WHERE num_registro LIKE '${num}' `,
      [req.body.num_registro],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).json({ error: error });
        }
        res.status(201).json({ response: resultado });
      }
    );
  });
});
//============ Pesquisa por data de um ponto de um colaborador ================
router.get("/:id/:data", loginTwo, (req, res, next) => {
  const  id = req.params.id
  const  data = req.params.data
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query (`SELECT * FROM pontos WHERE data LIKE '%${data}%' AND colaboradores_idcolaboradores = ${id}`, 
    [data],
    (error, resultado, field) => {
      conn.release();
      if (error) {
        return res.status(500).json({ error: error });
      }
      res.status(201).json({ response: resultado });
    });
  });
});

module.exports = router;
