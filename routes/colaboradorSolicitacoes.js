const express = require("express");
const router = express.Router();
const mysql = require("../mysql").pool;
const loginTwo = require("../middleware/loginTwo.js");
const dayjs = require("dayjs");


//====== Solicitação de ajuste de Pontos =========
router.post("/", loginTwo, (req, res, next) => {
    mysql.getConnection((error, conn) => {
      if (error) {
        return res.status(500).send({ error: error });
      }
      conn.query(
        `INSERT INTO solicitacoes (data, entrada, saida, observacao, colaboradores_idcolaboradores, pontos_num_registro) VALUES (?,?,?,?,?,?)`,
        [
          req.body.data,
          req.body.entrada,
          req.body.saida,
          req.body.observacao,
          req.body.colaboradores_idcolaboradores,
          req.body.pontos_num_registro,
        ],
        (error, resultado, fied) => {
          conn.release();
          error && res.status(500).send({ error: error });
  
          res.status(201).send({
            mensagem: "Solicitação feita com sucesso!",
            id: resultado.insertId,
          });
        }
      );
    });
  });

//======= Retorna as Solicitações de um único colaborador ==============
router.get("/:id", loginTwo, (req, res, next) => {
    const { id } = req.params;
    mysql.getConnection((error, conn) => {
      if (error) {
        return res.status(500).send({ error: error });
      }
      conn.query(
        `SELECT pontos.data AS pontosData, pontos.entrada AS pontosEntrada, pontos.saida AS pontosSaida, solicitacoes.entrada, solicitacoes.saida, solicitacoes.id, .solicitacoes.pontos_num_registro, solicitacoes.data, solicitacoes.observacao, solicitacoes.edit FROM solicitacoes JOIN pontos ON solicitacoes.pontos_num_registro=pontos.num_registro WHERE solicitacoes.colaboradores_idcolaboradores LIKE '${id}' order by data asc`,
        [req.body.colaboradores_idcolaboradores],
        (error, resultado, field) => {
          conn.release();
          if (error) {
            return res.status(500).send({ error: error });
          }
          res.status(201).send({ response: resultado });
        }
      );
    });
  });

  module.exports = router;