const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");

const app = express();
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const rotaColaboradores = require("./routes/colaboradores");
const rotaTarefasProjetos = require("./routes/tarefasProjetos");
const rotaSolicitacoes = require("./routes/solicitacoes");
const rotaPontos = require("./routes/pontos");
const rotaColaboradorPontos = require("./routes/colaboradorPontos");
const rotaColaboradorSolicitacoes = require("./routes/colaboradorSolicitacoes");



app.use(cors({ origin: true, credentials: true }));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Header",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );

  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).send({});
  }

  next();
});

app.use("/pontos", rotaPontos);
app.use("/colaborador/pontos", rotaColaboradorPontos);
app.use("/colaborador/solicitacoes", rotaColaboradorSolicitacoes);
app.use("/tarefasProjetos", rotaTarefasProjetos);
app.use("/solicitacoes", rotaSolicitacoes);
app.use("/colaboradores", rotaColaboradores);

// quando não encontra rota
app.use((req, res, next) => {
  const erro = new Error("ROTA NAO ENCONTRADA");
  erro.status = 404;
  next(erro);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  return res.send({
    erro: {
      mensagem: error.message,
    },
  });
});

module.exports = app;
